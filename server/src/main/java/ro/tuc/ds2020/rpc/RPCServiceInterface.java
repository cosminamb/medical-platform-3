package ro.tuc.ds2020.rpc;


import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.MedicationPlan;

import java.util.List;
import java.util.UUID;

/*
    https://howtodoinjava.com/spring-boot2/spring-remoting-rmi-hessian/
 */
public interface RPCServiceInterface {

    String testConnection(String msg);

    MedicationPlanDTO sendPatientsMedicationPlan(UUID patientId);

    void informServerPatientTookMedication(String message);

    void informServerNotTakenMedications(String message);

}
