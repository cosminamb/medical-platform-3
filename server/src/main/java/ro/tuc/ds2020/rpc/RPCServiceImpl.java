package ro.tuc.ds2020.rpc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RPCServiceImpl implements RPCServiceInterface {

    private static final Logger LOGGER = LoggerFactory.getLogger(RPCServiceImpl.class);


    private final PatientRepository patientRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public RPCServiceImpl(PatientRepository patientRepository, MedicationPlanRepository medicationPlanRepository) {
        this.patientRepository = patientRepository;
        this.medicationPlanRepository = medicationPlanRepository;
    }

    @Override
    public String testConnection(String msg) {
        System.out.println("Test Connection Method in Server");
        return "Message from server: Got " + msg + " message from client" + "\n";
    }

    @Override
    public MedicationPlanDTO sendPatientsMedicationPlan(UUID patientId) {
        Optional<Patient> patientOpt = patientRepository.findById(patientId);
        if(!patientOpt.isPresent()){
            LOGGER.error("Patient with id {} was not found in db", patientId);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + patientId);
        }
        Patient patient = patientOpt.get();
        MedicationPlan medicationPlan = patient.getMedicationPlan();
        System.out.println("med plan is: " +medicationPlan);
        return MedicationPlanBuilder.toMedicationPlanDTO(medicationPlan);
    }

    @Override
    public void informServerPatientTookMedication(String message) {
        System.out.println(message);
    }

    @Override
    public void informServerNotTakenMedications(String message) {
        System.out.println(message);

    }
}
