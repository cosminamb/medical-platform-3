package ro.tuc.ds2020.rpc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;

@Configuration
public class HessianConfigExporter {

    @Autowired
    RPCServiceImpl rpcService;

    @Bean(name = "/hessian_exporter")
    public RemoteExporter createHessianExporter(){
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(rpcService);
        exporter.setServiceInterface(RPCServiceInterface.class);
        return exporter;
    }
}
