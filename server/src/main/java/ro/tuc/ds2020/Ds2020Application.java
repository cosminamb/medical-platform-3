package ro.tuc.ds2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.validation.annotation.Validated;
import ro.tuc.ds2020.rpc.RPCServiceInterface;

import java.util.TimeZone;

@SpringBootApplication
@Validated
public class Ds2020Application extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
       // application.headless(false); /*to instantiate view*/
        return application.sources(Ds2020Application.class); 
    }

    public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC+2"));
        SpringApplication.run(Ds2020Application.class, args);
//        RPCServiceInterface serviceInterface = context.getBean(RPCServiceInterface.class);
//        System.out.println(serviceInterface.testConnection("Cosmina"));
//        new Server(8080).start();
    }
}
