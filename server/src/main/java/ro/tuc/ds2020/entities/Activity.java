package ro.tuc.ds2020.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Activity implements Serializable {

    private static final long serialVersionUID = -8140050461488382476L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "uuid")
    private UUID id;
    @Column
    private Date startTime;
    @Column
    private Date endTime;
    @Column
    private String activityName;

    @ManyToOne
    private Patient patient;

    public Activity(Date start, Date end, String activityName) {
        this.startTime = start;
        this.endTime = end;
        this.activityName = activityName;
    }
}
