package ro.tuc.ds2020.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.util.Date;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Doctor extends Person {

    private static final long serialVersionUID = -3461680376367923035L;

    public Doctor(String name, String address, int age, Date birthdate, char gender) {
        super(name, address, age, birthdate, gender);
    }
}
