package ro.tuc.ds2020.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Patient extends Person {


    private static final long serialVersionUID = -732639272183754652L;
    @Column(name = "medicalRecord", nullable = false)
    private String medicalRecord;

    @ManyToOne(cascade = CascadeType.REMOVE)
    private Caregiver caregiver;

    @OneToOne(orphanRemoval = true) //deletes ALSO the medication plan
    private MedicationPlan medicationPlan;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL) // orphanRemoval = false, fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private List<Activity> activityList;

    public Patient(String name, String address, int age, Date birthdate, char gender, String medicalRecord) {
        super(name, address, age, birthdate, gender);
        this.medicalRecord = medicalRecord;
    }

    public Patient(UUID id, String name, String address, int age, Date birthdate, char gender, String medicalRecord) {
        super(id, name, address, age, birthdate, gender);
        this.medicalRecord = medicalRecord;
    }

    public Patient(UUID id, String name, String address, int age, Date birthdate, char gender, String medicalRecord, Caregiver caregiver, MedicationPlan medicationPlan) {
        super(id, name, address, age, birthdate, gender);
        this.medicalRecord = medicalRecord;
        this.caregiver = caregiver;
        this.medicationPlan = medicationPlan;
    }
}
