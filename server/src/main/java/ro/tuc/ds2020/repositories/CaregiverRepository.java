package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.Caregiver;

import java.util.List;
import java.util.UUID;

public interface CaregiverRepository extends JpaRepository<Caregiver, UUID> {

    List<Caregiver> findByName(String name);

    Caregiver findCaregiverById(UUID id);

    Caregiver findCaregiverByName(String name);

    void deleteById(UUID id);

    void deleteByName(String name);

    void deleteAll();

}
