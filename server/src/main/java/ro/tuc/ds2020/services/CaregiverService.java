package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.repositories.CaregiverRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class CaregiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public List<CaregiverDTO> findCaregivers() {
        List<Caregiver> CaregiverList = caregiverRepository.findAll();
        return CaregiverList.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }

    public CaregiverDetailsDTO findCaregiverById(UUID id) {
        Optional<Caregiver> optionalCaregiver = caregiverRepository.findById(id);
        if (!optionalCaregiver.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDetailsDTO(optionalCaregiver.get());
    }

    public UUID insert(CaregiverDetailsDTO CaregiverDTO) {
        Caregiver Caregiver = CaregiverBuilder.toEntity(CaregiverDTO);
        Caregiver = caregiverRepository.save(Caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", Caregiver.getId());
        return Caregiver.getId();
    }

    public UUID deleteCaregiverById(UUID caregiverId) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(caregiverId);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", caregiverId);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + caregiverId);
        }
        caregiverRepository.deleteById(caregiverId);
        return caregiverId;
    }

    public CaregiverDetailsDTO updateCaregiver(CaregiverDetailsDTO caregiverDTO) {
        Caregiver fromDB = caregiverRepository.findCaregiverById(caregiverDTO.getId());
        fromDB.setName(caregiverDTO.getName());
        fromDB.setAddress(caregiverDTO.getAddress());
        fromDB.setAge(caregiverDTO.getAge());
        fromDB.setBirthdate(caregiverDTO.getBirthdate());
        //fromDB.setPatients(caregiverDTO.getPatients());

        Caregiver Caregiver = caregiverRepository.save(fromDB);
        return CaregiverBuilder.toCaregiverDetailsDTO(Caregiver);
    }

}
