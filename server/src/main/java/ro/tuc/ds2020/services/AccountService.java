package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.dtos.builders.AccountBuilder;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.DoctorRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);
    private final AccountRepository accountRepository;
    private final DoctorRepository doctorRepository;
    private final CaregiverRepository caregiverRepository;
    private final PatientRepository patientRepository;


    @Autowired
    public AccountService(AccountRepository accountRepository, DoctorRepository doctorRepository, CaregiverRepository caregiverRepository, PatientRepository patientRepository) {
        this.accountRepository = accountRepository;
        this.doctorRepository = doctorRepository;
        this.caregiverRepository = caregiverRepository;
        this.patientRepository = patientRepository;
    }

    public List<AccountDTO> findAccounts() {
        List<Account> accountList = accountRepository.findAll();
        return accountList.stream()
                .map(AccountBuilder::toAccountDTO)
                .collect(Collectors.toList());
    }

    public AccountDetailsDTO findAccountById(UUID id) {
        Optional<Account> optionalAccount = accountRepository.findById(id);
        if (!optionalAccount.isPresent()) {
            LOGGER.error("Account with id {} was not found in db", id);
            throw new ResourceNotFoundException(Account.class.getSimpleName() + " with id: " + id);
        }
        return AccountBuilder.toAccountDetailsDTO(optionalAccount.get());
    }

    public AccountDetailsDTO findAccount(String username, String password) {
        Optional<Account> account = accountRepository.findAccountByUsernameAndPassword(username, password);
        if (!account.isPresent()) {
            throw new ResourceNotFoundException(Account.class.getSimpleName() + " with username: " + username);
        }
        return AccountBuilder.toAccountDetailsDTO(account.get());
    }

    public UUID insert(AccountDetailsDTO accountDTO) {
        Account account = AccountBuilder.toEntity(accountDTO);
        if (getAccountType(accountDTO).getAccTy().equals("doctor")) {
            Doctor doctor = doctorRepository.findById(accountDTO.getAccountTypeId()).get();
            account.setAccountType(doctor);
        } else {
            if (getAccountType(accountDTO).getAccTy().equals("caregiver")) {
                Caregiver caregiver = caregiverRepository.findById(accountDTO.getAccountTypeId()).get();
                account.setAccountType(caregiver);
            } else {
                if (getAccountType(accountDTO).getAccTy().equals("patient")) {
                    Patient patient = patientRepository.findById(accountDTO.getAccountTypeId()).get();
                    account.setAccountType(patient);
                }
            }
        }
        account = accountRepository.save(account);
        LOGGER.debug("Account with id {} was inserted in db", account.getId());
        return account.getId();
    }

    public void deleteAccountById(UUID accountId) {
        accountRepository.deleteById(accountId);
    }

    public AccountDetailsDTO updateAccount(UUID accountId, AccountDetailsDTO accountDTO) {
        Account fromDB = accountRepository.findAccountById(accountId); //todo might not work
        fromDB.setUsername(accountDTO.getUsername());
        fromDB.setPassword(accountDTO.getPassword());

        Account account = accountRepository.save(fromDB);
        return AccountBuilder.toAccountDetailsDTO(account);
    }

    public AccountTypeDTO getAccountType(AccountDetailsDTO accountDetailsDTO) {
        String type = new String();
        AccountTypeDTO accountTypeDTO;
        Optional<Doctor> optionalDoctor = doctorRepository.findById(accountDetailsDTO.getAccountTypeId());
        if (optionalDoctor.isPresent()) {
            type = new String("doctor");
            Doctor doctor = optionalDoctor.get();
            DoctorDTO doctorDTO = DoctorBuilder.toDoctorDTO(doctor);
            accountTypeDTO = new AccountTypeDTO(doctorDTO, type);
        } else {
            Optional<Caregiver> optionalCaregiver = caregiverRepository.findById(accountDetailsDTO.getAccountTypeId());
            if (optionalCaregiver.isPresent()) {
                type = new String("caregiver");
                Caregiver caregiver = optionalCaregiver.get();
                CaregiverDTO caregiverDTO = CaregiverBuilder.toCaregiverDTO(caregiver);
                accountTypeDTO = new AccountTypeDTO(caregiverDTO, type);
            } else {
                Optional<Patient> optionalPatient = patientRepository.findById(accountDetailsDTO.getAccountTypeId());
                if (optionalPatient.isPresent()) {
                    type = new String("patient");
                    Patient patient = optionalPatient.get();
                    PatientDTO patientDTO = PatientBuilder.toPatientDTO(patient);
                    accountTypeDTO = new AccountTypeDTO(patientDTO, type);
                } else {
                    LOGGER.error("Entity with id {} was not found in db", accountDetailsDTO.getAccountTypeId());
                    throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + accountDetailsDTO.getAccountTypeId());

                }
            }
        }
        return accountTypeDTO;
    }


}
