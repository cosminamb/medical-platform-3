package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class MedicationPlanDTO extends RepresentationModel<MedicationPlanDTO> implements Serializable {

    private static final long serialVersionUID = -7847096507514143201L;

    private UUID id;
    private String intakeInterval;
    private String treatmentPeriod;
    private List<MedicationDTO> medicationDTO;

    public MedicationPlanDTO(UUID id, String intakeInterval, String treatmentPeriod, List<MedicationDTO> medicationDTO) {
        this.id = id;
        this.intakeInterval = intakeInterval;
        this.treatmentPeriod = treatmentPeriod;
        this.medicationDTO = medicationDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanDTO medicationDTO = (MedicationPlanDTO) o;
        return Objects.equals(intakeInterval, medicationDTO.intakeInterval) &&
                Objects.equals(treatmentPeriod, medicationDTO.treatmentPeriod);
    }

    @Override
    public int hashCode() {
        return Objects.hash(intakeInterval, treatmentPeriod);
    }
}
