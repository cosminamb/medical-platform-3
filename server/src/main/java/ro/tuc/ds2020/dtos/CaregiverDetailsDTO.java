package ro.tuc.ds2020.dtos;

import lombok.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class CaregiverDetailsDTO extends PersonDetailsDTO {

    private UUID id;
    private List<PatientDTO> patientDTOList = new ArrayList<>();

    @Builder
    public CaregiverDetailsDTO(String name, String address, int age, Date birthdate, char gender, UUID id, List<PatientDTO> patientDTOList) {
        super(name, address, age, birthdate, gender);
        this.id = id;
        this.patientDTOList = patientDTOList;
    }


}
