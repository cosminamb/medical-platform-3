package ro.tuc.ds2020.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class PatientDTO extends PersonDTO {

    private String medicalRecord;
    private MedicationPlanDTO medicationPlanDTO;

    @Builder
    public PatientDTO(UUID id, String name, int age, Date birthdate, String address, char gender, String medicalRecord, MedicationPlanDTO medicationPlanDTO) {
        super(id, name, age, birthdate, address, gender);
        this.medicalRecord = medicalRecord;
        this.medicationPlanDTO = medicationPlanDTO;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.getName(), super.getAge(), medicalRecord);
    }
}
