package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class DoctorDetailsDTO extends PersonDetailsDTO {

    public DoctorDetailsDTO(String name, String address, int age, Date birthdate, char gender) {
//        super(name, address, age, birthdate, gender);
    }

    public DoctorDetailsDTO(UUID id, String name, String address, int age, Date birthdate, char gender) {
//        super(id, name, address, age, birthdate, gender);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
