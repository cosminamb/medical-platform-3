package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class MedicationDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String sideEffects;
    @NotNull
    private String dosage;

    private UUID medicationPlanId;


    public MedicationDetailsDTO(String name, String sideEffects, String dosage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public MedicationDetailsDTO(UUID id, String name, String sideEffects, String dosage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public MedicationDetailsDTO(UUID id, String name, String sideEffects, String dosage, UUID medicationPlanId) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
        this.medicationPlanId = medicationPlanId;
    }

    public MedicationDetailsDTO(String name, String sideEffects, String dosage, UUID medicationPlanId) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
        this.medicationPlanId = medicationPlanId;
    }

    public boolean hasMedicationPlanId() {
        return this.medicationPlanId != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDetailsDTO medicationDTO = (MedicationDetailsDTO) o;
        return Objects.equals(name, medicationDTO.name) &&
                Objects.equals(sideEffects, medicationDTO.sideEffects) &&
                Objects.equals(dosage, medicationDTO.dosage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sideEffects, dosage);
    }

}
