package ro.tuc.ds2020.dtos.builders;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.entities.Activity;

@NoArgsConstructor
public class ActivityBuilder {

    public static Activity toEntity(ActivityDTO activityDTO) {
        return new Activity(activityDTO.getStartTime(), activityDTO.getEndTime(), activityDTO.getActivityName());
    }
}
