package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class MedicationDTO extends RepresentationModel<MedicationDTO> implements Serializable {

    private static final long serialVersionUID = -983356986798961993L;

    private UUID id;
    private String name;
    private String sideEffects;
    private String dosage;


    public MedicationDTO(UUID id, String name, String sideEffects, String dosage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO medicationDTO = (MedicationDTO) o;
        return Objects.equals(name, medicationDTO.name) &&
                Objects.equals(sideEffects, medicationDTO.sideEffects) &&
                Objects.equals(dosage, medicationDTO.dosage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sideEffects, dosage);
    }
}
