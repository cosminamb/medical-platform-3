package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.entities.Account;

public class AccountBuilder {

    private AccountBuilder() {
    }

    public static AccountDTO toAccountDTO(Account account) {
        return new AccountDTO(account.getId(), account.getUsername(), account.getPassword());
    }

    public static AccountDetailsDTO toAccountDetailsDTO(Account account) {
        return new AccountDetailsDTO(account.getId(), account.getUsername(), account.getPassword(), account.getAccountType().getId());
    }

    public static Account toEntity(AccountDetailsDTO accountDetailsDTO) {
        return new Account(accountDetailsDTO.getId(),
                accountDetailsDTO.getUsername(),
                accountDetailsDTO.getPassword());
    }

}
