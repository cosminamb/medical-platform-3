package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class AccountDTO extends RepresentationModel<AccountDTO> {

    private UUID id;
    private String username;
    private String password;

    public AccountDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public AccountDTO(UUID id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDTO accountDTO = (AccountDTO) o;
        return Objects.equals(username, accountDTO.username) &&
                Objects.equals(password, accountDTO.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }
}
