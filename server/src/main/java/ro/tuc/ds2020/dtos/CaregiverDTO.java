package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class CaregiverDTO extends PersonDTO {

    public CaregiverDTO(UUID id, String name, int age, Date birthdate, String address, char gender) {
        super(id, name, age, birthdate, address, gender);
    }


    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.getName(), super.getAge());
    }


}

