package ro.tuc.ds2020.dtos;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class MedicationPlanIdDTO {

    private UUID id;
    @NotNull
    private String intakeInterval;
    @NotNull
    private String treatmentPeriod;

    private List<UUID> medicationIds;

    private UUID patientId;


    @Builder
    public MedicationPlanIdDTO(UUID id, String intakeInterval, String treatmentPeriod, List<UUID> medicationId) {
        this.id = id;
        this.intakeInterval = intakeInterval;
        this.treatmentPeriod = treatmentPeriod;
        this.medicationIds = medicationId;
    }

    public boolean hasMedicationIds() {
        return (this.medicationIds != null && this.medicationIds.size() > 0);
    }

}

