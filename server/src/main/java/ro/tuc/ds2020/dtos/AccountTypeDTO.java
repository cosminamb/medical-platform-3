package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class AccountTypeDTO {

    private PersonDTO person;
    private String accTy;

    public AccountTypeDTO(String accTy) {
        this.accTy = accTy;
    }

    public AccountTypeDTO(PersonDTO person, String accTy) {
        this.person = person;
        this.accTy = accTy;
    }
}
