package ro.tuc.ds2020.dtos;

import lombok.*;

import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class PatientDetailsDTO extends PersonDetailsDTO {

    private UUID id;
    private String medicalRecord;
    private UUID caregiverId;
    private UUID medicationPlanId;


    @Builder
    public PatientDetailsDTO(UUID id, String name, String address, int age, Date birthdate, char gender, String medicalRecord, UUID caregiverId, UUID medicationPlanId) {
        super(name, address, age, birthdate, gender);
        this.id = id;
        this.medicalRecord = medicalRecord;
        this.caregiverId = caregiverId;
        this.medicationPlanId = medicationPlanId;
    }

    public boolean hasCaregiverId() {
        return this.caregiverId != null;
    }

    public boolean hasMedicationPlanId() {
        return this.medicationPlanId != null;
    }

}
