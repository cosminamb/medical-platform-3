package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.services.ActivityService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
//
//@RestController
//@CrossOrigin
//@RequestMapping(value = "/anomActivity")
@Controller
public class ActivityController {
    
    private final ActivityService activityService;

//    @Autowired
    public ActivityController(ActivityService activityService) {
        this.activityService = activityService;
    }



    @MessageMapping("/notification.send")
    @SendTo("/topic/public")
    public ActivityDTO sendActivities(@Payload ActivityDTO activityDTO){
        System.out.println("send activity in server");
        return activityDTO;
    }

//    @GetMapping()
//    public ResponseEntity<List<ActivityDTO>> getActivities() {
//        List<ActivityDTO> dtos = activityService.findActivities();
//        for (ActivityDTO dto : dtos) {
//            Link activityLink = linkTo(methodOn(ActivityController.class)
//                    .getActivity(dto.getId())).withRel("activityDetails");
//            dto.add(activityLink);
//        }
//        return new ResponseEntity<>(dtos, HttpStatus.OK);
//    }
/*
    @PostMapping()
    public ResponseEntity<UUID> insertActivity(@RequestBody ActivityDTO activityDTO) {
        UUID activityID = activityService.insert(activityDTO);
        return new ResponseEntity<>(activityID, HttpStatus.CREATED);
    }*/

//    @GetMapping(value = "/{id}")
//    public ResponseEntity<ActivityDetailsDTO> getActivity(@PathVariable("id") UUID activityId) {
//        ActivityDetailsDTO dto = activityService.findActivityById(activityId);
//        return new ResponseEntity<>(dto, HttpStatus.OK);
//    }

}
