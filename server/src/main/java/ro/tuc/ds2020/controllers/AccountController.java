package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.services.AccountService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/account")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping()
    public ResponseEntity<List<AccountDTO>> getAccounts() {
        List<AccountDTO> dtos = accountService.findAccounts();
        for (AccountDTO dto : dtos) {
            Link accountLink = linkTo(methodOn(AccountController.class)
                    .getAccount(dto.getId())).withRel("accountDetails");
            dto.add(accountLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertAccount(@Valid @RequestBody AccountDetailsDTO accountDTO) {
        UUID accountID = accountService.insert(accountDTO);
        return new ResponseEntity<>(accountID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AccountDetailsDTO> getAccount(@PathVariable("id") UUID accountId) {
        AccountDetailsDTO dto = accountService.findAccountById(accountId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    @PutMapping(value = "/{id}")
    public ResponseEntity<AccountDetailsDTO> updateAccount(@PathVariable("id") UUID accountId, @Valid @RequestBody AccountDetailsDTO accountDTO) {
        AccountDetailsDTO updatedAccount = accountService.updateAccount(accountId, accountDTO);
        return new ResponseEntity<>(updatedAccount, HttpStatus.OK);

    }

    @DeleteMapping(value = "/{id}")
    void deleteAccountById(@PathVariable("id") UUID accountId) {
        accountService.deleteAccountById(accountId);
    }

}