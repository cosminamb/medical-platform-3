package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.MedicationDetailsDTO;
import ro.tuc.ds2020.services.MedicationService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MedicationControllerUnitTest extends Ds2020TestConfig {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicationService service;

    @Test
    public void insertMedicationTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        MedicationDetailsDTO medicationDTO = new MedicationDetailsDTO("Medication", "No side effects", "3");

        mockMvc.perform(post("/medication")
                .content(objectMapper.writeValueAsString(medicationDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }


    @Test
    public void insertMedicationTestFailsDueToNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        MedicationDetailsDTO medicationDTO = new MedicationDetailsDTO("Medication", null, "3");

        mockMvc.perform(post("/medication")
                .content(objectMapper.writeValueAsString(medicationDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

}