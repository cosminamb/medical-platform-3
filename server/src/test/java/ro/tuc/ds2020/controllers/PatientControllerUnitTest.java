package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.services.PatientService;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PatientControllerUnitTest extends Ds2020TestConfig {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatientService service;


    @Test
    public void insertPatientTestFailsDueToAge() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        PatientDetailsDTO patientDTO = PatientDetailsDTO.builder()
                .name("John")
                .address("Somewhere Else street")
                .birthdate(new Date(1998, 12, 06))
                .age(17)
                .gender('M')
                .medicalRecord("No medical record")
                .build();

        mockMvc.perform(post("/patient")
                .content(objectMapper.writeValueAsString(patientDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void insertPatientTestFailsDueToNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        PatientDetailsDTO patientDTO = PatientDetailsDTO.builder()
                .name("John")
                .address(null)
                .birthdate(new Date(1998, 12, 06))
                .age(22)
                .gender('M')
                .medicalRecord("No medical record")
                .build();
        mockMvc.perform(post("/patient")
                .content(objectMapper.writeValueAsString(patientDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }
}