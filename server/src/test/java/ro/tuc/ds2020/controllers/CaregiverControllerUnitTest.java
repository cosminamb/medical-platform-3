package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.services.CaregiverService;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CaregiverControllerUnitTest extends Ds2020TestConfig {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CaregiverService service;

    @Test
    public void insertCaregiverTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        CaregiverDetailsDTO caregiverDTO = CaregiverDetailsDTO.builder()
                .name("John")
                .address("Somewhere Else street")
                .birthdate(new Date(1998, 12, 06))
                .age(22)
                .gender('M')
                .build();
        mockMvc.perform(post("/caregiver")
                .content(objectMapper.writeValueAsString(caregiverDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    @Test
    public void insertCaregiverTestFailsDueToAge() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        CaregiverDetailsDTO caregiverDTO = CaregiverDetailsDTO.builder()
                .name("John")
                .address("Somewhere Else street")
                .birthdate(new Date(1998, 12, 06))
                .age(22)
                .gender('M')
                .build();
        mockMvc.perform(post("/caregiver")
                //.content(objectMapper.writeValueAsString(caregiverDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void insertCaregiverTestFailsDueToNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        CaregiverDetailsDTO caregiverDTO = CaregiverDetailsDTO.builder()
                .name("John")
                .address(null)
                .birthdate(new Date(2003, 12, 06))
                .age(22)
                .gender('M')
                .build();

        mockMvc.perform(post("/caregiver")
                .content(objectMapper.writeValueAsString(caregiverDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

}