package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.MedicationPlanDetailsDTO;
import ro.tuc.ds2020.services.MedicationPlanService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class MedicationPlanControllerUnitTest extends Ds2020TestConfig {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicationPlanService service;

    @Test
    public void insertMedicationPlanTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        MedicationPlanDetailsDTO medicationPlanDTO = new MedicationPlanDetailsDTO("8 hrs", "5 days");

        mockMvc.perform(post("/medicationPlan")
                .content(objectMapper.writeValueAsString(medicationPlanDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    @Test
    public void insertMedicationPlanTestFailsDueToNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        MedicationPlanDetailsDTO medicationPlanDTO = new MedicationPlanDetailsDTO("8 hrs", null);

        mockMvc.perform(post("/medicationPlan")
                .content(objectMapper.writeValueAsString(medicationPlanDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

}