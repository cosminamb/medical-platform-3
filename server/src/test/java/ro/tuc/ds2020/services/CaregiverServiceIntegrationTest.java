package ro.tuc.ds2020.services;


import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;

import java.util.Date;
import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertEquals;


@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class CaregiverServiceIntegrationTest extends Ds2020TestConfig {


    @Autowired
    CaregiverService caregiverService;

    @Test
    public void testGetCorrect() {
        List<CaregiverDTO> caregiverDTOList = caregiverService.findCaregivers();
        assertEquals("Test Insert Caregiver", 1, caregiverDTOList.size());
    }

//    @Test
//    public void testInsertCorrectWithGetById() {
//        CaregiverDetailsDTO p = CaregiverDetailsDTO.builder()
//                .name("John")
//                .address("Somewhere Else street")
//                .birthdate(new Date(1998, 12, 06))
//                .age(22)
//                .gender('M')
//                .build();
//        UUID insertedID = caregiverService.insert(p);
//
//        CaregiverDetailsDTO insertedCaregiver = CaregiverDetailsDTO.builder()
//                .id(insertedID)
//                .name(p.getName())
//                .address(p.getAddress())
//                .birthdate(p.getBirthdate())
//                .age(p.getAge())
//                .gender(p.getGender())
//                .build();
//        CaregiverDetailsDTO fetchedCaregiver = caregiverService.findCaregiverById(insertedID);
//
//        assertEquals("Test Inserted Caregiver", insertedCaregiver, fetchedCaregiver);
//    }

    @Test
    public void testInsertCorrectWithGetAll() {
        CaregiverDetailsDTO p = CaregiverDetailsDTO.builder()
                .name("John")
                .address("Somewhere Else street")
                .birthdate(new Date(1998, 12, 06))
                .age(22)
                .gender('M')
                .build();
        caregiverService.insert(p);

        List<CaregiverDTO> caregiverDTOList = caregiverService.findCaregivers();
        assertEquals("Test Inserted Caregivers", 2, caregiverDTOList.size());
    }


}