package ro.tuc.ds2020.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class PatientServiceIntegrationTest extends Ds2020TestConfig {


    @Autowired
    PatientService patientService;

    @Test
    public void testGetCorrect() {
        List<PatientDTO> patientDTOList = patientService.findPatients();
        assertEquals("Test Insert Patient", 1, patientDTOList.size());
    }

    @Test
    public void testInsertCorrectWithGetById() {
        PatientDetailsDTO p = PatientDetailsDTO.builder()
                .name("John")
                .address("Somewhere Else street")
                .birthdate(new Date(1998, 12, 06))
                .age(20)
                .gender('M')
                .medicalRecord("No medical record")
                .build(); //new PatientDetailsDTO("John", "Somewhere Else street", 22, new Date(1998, 12, 06), 'M', "None");
        UUID insertedID = patientService.insert(p);

        PatientDetailsDTO insertedPatient = PatientDetailsDTO.builder()
                .id(insertedID)
                .name(p.getName())
                .address(p.getAddress())
                .birthdate(p.getBirthdate())
                .age(p.getAge())
                .gender(p.getGender())
                .medicalRecord(p.getMedicalRecord())
                .build();
        //new PatientDetailsDTO(insertedID, ,, , p.getBirthdate(), , );
        PatientDetailsDTO fetchedPatient = patientService.findPatientById(insertedID);

        assertEquals("Test Inserted Patient", insertedPatient, fetchedPatient);
    }

    @Test
    public void testInsertCorrectWithGetAll() {
        PatientDetailsDTO p = PatientDetailsDTO.builder()
                .name("John")
                .address("Somewhere Else street")
                .birthdate(new Date(1998, 12, 06))
                .age(20)
                .gender('M')
                .medicalRecord("No medical record")
                .build();  //new PatientDetailsDTO("John", "Somewhere Else street", 22, new Date(1998, 12, 06), 'M', "None");
        patientService.insert(p);

        List<PatientDTO> patientDTOList = patientService.findPatients();
        assertEquals("Test Inserted Patients", 2, patientDTOList.size());
    }

}