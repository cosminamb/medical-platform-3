package ro.tuc.ds2020.gui;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.rpc.MessageServer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.time.LocalTime;
import java.util.List;

@Component
public class MedicationsView extends JFrame {

    JPanel medicationsPanel = new JPanel();
    JLabel currentTimeLabel = new JLabel();
    JPanel medicationsTablePanel = new JPanel();
    JButton takeButton = new JButton("Take");
    JTable timetable = new JTable();
    @Autowired
    MessageServer messageServer;

    public MedicationsView(MessageServer messageServer) { //throws HeadlessException {
        this.messageServer = messageServer;
        initView();
        showMedicationsToTake();
    }

    private void initView() {
        this.setMinimumSize(new Dimension(700, 500));
        this.setTitle("Medication plan");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        this.setVisible(true);
        medicationsPanel.setVisible(true);
        currentTimeLabel.setText(messageServer.getCurrentTime());
        medicationsPanel.add(currentTimeLabel);

        this.add(medicationsPanel);
    }

    @Scheduled(fixedDelay = 1000)
    private void refreshTimeDisplayed() {
        medicationsPanel.remove(currentTimeLabel);
        currentTimeLabel.setText(messageServer.getCurrentTime());
        medicationsPanel.add(currentTimeLabel);
        medicationsPanel.revalidate();
        medicationsPanel.repaint();
    }

    public DefaultTableModel createTableModel(MedicationPlanDTO medicationPlanDTO) {
        List<MedicationDTO> toShow = medicationPlanDTO.getMedicationDTO();
        Object[][] data = new Object[toShow.size()][4];
        String[] columnNames = {"Name", "Dosage", "Side Effects", "Intake interval"};
        //data[0] = toShow.toArray();
        int i = 0;
        LocalTime start = LocalTime.MIN;
        for (MedicationDTO dto : toShow) {
            data[i][0] = dto.getName();
            data[i][1] = dto.getDosage();
            data[i][2] = dto.getSideEffects();
            data[i][3] = createInterval(start = start.plusHours(Long.parseLong(medicationPlanDTO.getIntakeInterval())));
            i++;
//            System.out.println(dto.getName());
        }
        return new DefaultTableModel(data, columnNames);
    }

    public void showMedicationsToTake(){
        MedicationPlanDTO medicationPlanDTO = messageServer.getPatientsMedicationPlan();
        DefaultTableModel defaultTableModel = createTableModel(medicationPlanDTO);
        //JTable timetable = new JTable(defaultTableModel);
        timetable.setModel(defaultTableModel);
        JScrollPane scrollPane = new JScrollPane(timetable);
        scrollPane.setBounds(0, 0, 450, 300);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        medicationsTablePanel.add(scrollPane);
        medicationsTablePanel.setBounds(0, 50, 460, 300);
        medicationsTablePanel.setVisible(true);
        medicationsPanel.add(medicationsTablePanel);
        medicationsPanel.add(takeButton);
        takeButton.addActionListener(e-> removeSelectedMedication(timetable, defaultTableModel));
        medicationsPanel.revalidate();
        medicationsPanel.repaint();

    }

    private void removeSelectedMedication(JTable table, DefaultTableModel defaultTableModel) {
        int selectedRow = table.getSelectedRow();
        if(selectedRow != -1) {
            if (medicationCanBeTaken((String) table.getValueAt(selectedRow, 3))) { //column = 3 is intake interval from table
                messageServer.getRpcServiceInterface().informServerPatientTookMedication("Medication taken: " +
                        getMedicationDetails(table,selectedRow));
                defaultTableModel.removeRow(selectedRow);
            }else{
                JOptionPane.showMessageDialog(new JFrame(), "You are not supposed to take this medication now!");
            }
        }
    }

    public boolean medicationCanBeTaken(String intakeInterval){
        String[] hours = intakeInterval.split("-");
        LocalTime beginInterval = LocalTime.parse(hours[0]);
        LocalTime endInterval = LocalTime.parse(hours[1]);
        return beginInterval.isBefore(LocalTime.now()) && endInterval.isAfter(LocalTime.now());
    }

    public String createInterval(LocalTime time){
        LocalTime begin = time.minusMinutes(125);
        LocalTime end = time.plusMinutes(-105);
        return begin + "-" + end;
    }

    public String getMedicationDetails(JTable table, int selectedRow){
        return  table.getValueAt(selectedRow, 0) + " with dosage=" +
                table.getValueAt(selectedRow, 1) + " and side effects= " +
                table.getValueAt(selectedRow, 2);
    }

    @Scheduled(fixedDelay = 1000)
    public void checkNotTakenMedications(){
        String notTakenMedications = new String();
        //System.out.println("rows " + timetable.getRowCount());
        String now = messageServer.getCurrentTime();
        LocalTime nowTime = LocalTime.parse(now);
        for(int i=0; i<timetable.getRowCount(); i++){
            String endInterval = timetable.getValueAt(i, 3).toString().split("-")[1];
            LocalTime endIntervalTime = LocalTime.parse(endInterval);
            if(endIntervalTime.isAfter(nowTime) && endIntervalTime.minusSeconds(2).isBefore(nowTime)){
                notTakenMedications += "Alert!!! Medication not taken: " + getMedicationDetails(timetable, i);
            }
        }
        if(!notTakenMedications.isEmpty()){
            messageServer.getRpcServiceInterface().informServerNotTakenMedications(notTakenMedications);
        }
    }
}
