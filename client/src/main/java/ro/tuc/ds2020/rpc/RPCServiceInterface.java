package ro.tuc.ds2020.rpc;


import ro.tuc.ds2020.dtos.MedicationPlanDTO;

import java.util.List;
import java.util.UUID;

public interface RPCServiceInterface {

    String testConnection(String msg);

    MedicationPlanDTO sendPatientsMedicationPlan(UUID patientId);

    void informServerPatientTookMedication(String message);

    void informServerNotTakenMedications(String message);
}
