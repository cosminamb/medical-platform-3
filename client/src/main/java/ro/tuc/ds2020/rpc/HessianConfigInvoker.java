package ro.tuc.ds2020.rpc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;

@Configuration
public class HessianConfigInvoker {

    @Bean
    public HessianProxyFactoryBean createHessianInvoker(){
        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
//        invoker.setServiceUrl("http://localhost:8080/hessian_exporter");
        invoker.setServiceUrl("https://balus-cosmina-a3-server.herokuapp.com/hessian_exporter");
        invoker.setServiceInterface(RPCServiceInterface.class);
        return invoker;
    }
}
