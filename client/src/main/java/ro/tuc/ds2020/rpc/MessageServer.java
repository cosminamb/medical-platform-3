package ro.tuc.ds2020.rpc;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Getter
@Service
public class MessageServer {

    RPCServiceInterface rpcServiceInterface;

    @Autowired
    public MessageServer(RPCServiceInterface rpcServiceInterface) {
        this.rpcServiceInterface = rpcServiceInterface;
    }

//    @Scheduled(fixedDelay = 10000)
//    public void testConnectionToServer(){
//        System.out.println(rpcServiceInterface.testConnection("Cosmina Balus"));
//    }

    @Scheduled(cron = "0 0 20 * * *") /*download medication plan every day at 20 hour https://stackoverflow.com/questions/35337198/cron-expression-for-spring-scheduler-run-only-once-a-year*/
    public MedicationPlanDTO getPatientsMedicationPlan(){
//        MedicationPlanDTO medicationPlanDTO = rpcServiceInterface.sendPatientsMedicationPlan(UUID.fromString("6cf49ac5-92ae-44e6-a994-9188f89df9fc")); //harcoded patient id local
        MedicationPlanDTO medicationPlanDTO = rpcServiceInterface.sendPatientsMedicationPlan(UUID.fromString("7b5fda5b-edf7-4537-aba7-c1fe9b68a223")); //harcoded patient id heroku
//        for(MedicationDTO dto : medicationPlanDTO.getMedicationDTO()){
//            System.out.println("MED: " + dto.getId() + " " + dto.getName() +" " + dto.getDosage() + " " + dto.getSideEffects());
//        }
//        System.out.println("MP IS " + medicationPlanDTO.getId() + " " + medicationPlanDTO.getIntakeInterval() +" "+ medicationPlanDTO.getTreatmentPeriod());
        return medicationPlanDTO;
    }

    public String getCurrentTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        return formatter.format(Calendar.getInstance().getTime());
    }



}
