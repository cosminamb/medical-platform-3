package ro.tuc.ds2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import ro.tuc.ds2020.rpc.RPCServiceInterface;

@SpringBootApplication
@EnableScheduling
public class Ds2020Application {

    public static void main(String[] args) {
//        SpringApplication.run(Ds2020Application.class, args);
        SpringApplicationBuilder builder = new SpringApplicationBuilder(Ds2020Application.class);
        builder.headless(false);
        builder.run(args);
//        RPCServiceInterface serviceInterface = context.getBean(RPCServiceInterface.class);
//        System.out.println(serviceInterface.testConnection("Cosmina"));
    }

}
