package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class MedicationPlanDetailsDTO {

    private UUID id;
    @NotNull
    private String intakeInterval;
    @NotNull
    private String treatmentPeriod;

    private List<MedicationDTO> medicationIds;

    public MedicationPlanDetailsDTO(String intakeInterval, String treatmentPeriod) {
        this.intakeInterval = intakeInterval;
        this.treatmentPeriod = treatmentPeriod;
    }

    public MedicationPlanDetailsDTO(UUID id, String intakeInterval, String treatmentPeriod) {
        this.id = id;
        this.intakeInterval = intakeInterval;
        this.treatmentPeriod = treatmentPeriod;
    }

    public MedicationPlanDetailsDTO(String intakeInterval, String treatmentPeriod, List<MedicationDTO> medicationId) {
        this.intakeInterval = intakeInterval;
        this.treatmentPeriod = treatmentPeriod;
        this.medicationIds = medicationId;
    }

    public MedicationPlanDetailsDTO(UUID id, String intakeInterval, String treatmentPeriod, List<MedicationDTO> medicationId) {
        this.id = id;
        this.intakeInterval = intakeInterval;
        this.treatmentPeriod = treatmentPeriod;
        this.medicationIds = medicationId;
    }

    public boolean hasMedicationIds() {
        return (this.medicationIds != null && this.medicationIds.size() > 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanDetailsDTO medicationPlanDTO = (MedicationPlanDetailsDTO) o;
        return Objects.equals(intakeInterval, medicationPlanDTO.intakeInterval) &&
                Objects.equals(treatmentPeriod, medicationPlanDTO.treatmentPeriod);
    }

    @Override
    public int hashCode() {
        return Objects.hash(intakeInterval, treatmentPeriod);
    }
}

